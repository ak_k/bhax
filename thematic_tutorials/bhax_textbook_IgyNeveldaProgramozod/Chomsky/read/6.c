#include <stdio.h>

int f(int a, int b)
{
	return 42;
}

int main()
{
	int a = 42;

	printf("%d %d", f(a, ++a), f(++a, a));

	return 0;
}