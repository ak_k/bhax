public class bbp_bench
{
    String hexaJegyek;

    public static long n16modk(int n, int k)
    {
        int t = 1;

        while (t <= n)
        {
            t *= 2;
        }

        if (t > n)
        {
            t /= 2;
        }

        long r = 1;

        while (true)
        {
            if (n >= t)
            {
                r = (16 * r) % k;
                n -= t;
            }

            t /= 2;

            if (t < 1)
            {
                break;
            }

            r = (r * r) % k;
        }

        return r;
    }

    public static double d16sj(int d, int j)
    {
        double d16sj = 0.0d;

        for (int i = 0; i <= d; ++i)
        {
            d16sj += (double)(n16modk(d - 1, 8 * i + j)) / (double)(8 * i + j);
        }

        for (int i = d + 1; i <= 2 * d; ++i)
        {
            d16sj += Math.pow(16.0, d - i) / (double)(8 * i + j);
        }

        return d16sj - Math.floor(d16sj);
    }

    public String toString()
    {
        return hexaJegyek;
    }

    public static void main(String[] args) 
    {
        double d16pi = 0.0d;
        
        double d16s1t = 0.0d;
        double d16s4t = 0.0d;
        double d16s5t = 0.0d;
        double d16s6t = 0.0d;

        int jegy = 0;

        long delta = System.currentTimeMillis();

        for (int d = 100000000; d < 100000001; ++d)
        {
            d16pi = 0.0;

            d16s1t = d16sj(d, 1);
            d16s4t = d16sj(d, 4);
            d16s5t = d16sj(d, 5);
            d16s6t = d16sj(d, 6);

            d16pi = 4.0d * d16s1t - 2.0d * d16s4t - d16s5t - d16s6t;

            d16pi = d16pi - Math.floor(d16pi);

            jegy = (int)Math.floor(16.0d * d16pi);
        }

        System.out.println(jegy);
        
        delta = System.currentTimeMillis() - delta;

        System.out.println(delta / 1000.0);
    }
}