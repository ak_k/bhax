using System;

public class bbp_bench
{
    public static double d16sj(int d, int j)
    {
        double d16sj = 0.0;

        for (int k = 0; k <= d; ++k)
        {
            d16sj += (double)n16modk(d - k, 8 * k + j) / (double)(8 * k + j);
        }

        return d16sj - Math.Floor(d16sj);
    }

    public static long n16modk(int n, int k)
    {
        int t = 1;

        while (t <= n)
        {
            t *= 2;
        }

        long r = 1;

        while (true)
        {
            if (n >= t)
            {
                r = (16 * r) % k;
                n -= t;
            }

            t /= 2;

            if (t < 1)
            {
                break;
            }

            r = (r * r) % k;
        }

        return r;
    }

    public static void Main(String[] args)
    {
        double d16pi = 0.0;

        double d16s1t = 0.0;
        double d16s4t = 0.0;
        double d16s5t = 0.0;
        double d16s6t = 0.0;

        int jegy = 0;

        DateTime kezd = DateTime.Now;

        for (int d = 100000000; d < 100000001; ++d)
        {
            d16pi = 0.0;

            d16s1t = d16sj(d, 1);
            d16s4t = d16sj(d, 4);
            d16s5t = d16sj(d, 5);
            d16s6t = d16sj(d, 6);

            d16pi = 4.0 * d16s1t - 2.0 * d16s4t - d16s5t - d16s6t;

            d16pi = d16pi - Math.Floor(d16pi);

            jegy = (int)Math.Floor(16.0 * d16pi);
        }

        Console.WriteLine(jegy);

        TimeSpan delta = DateTime.Now.Subtract(kezd);

        Console.WriteLine(delta.TotalMilliseconds / 1000.0);
    }
}