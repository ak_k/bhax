#include <iostream>

class szulo
{
    public: 
        void szuloUzen()
        {
            std::cout << "Parent";
        }
};

class gyerek
{
    public:
        void gyerekUzen()
        {
            std::cout << "Child";
        }
};

class szulogyerek
{
    int main()
    {
        szulo sz = new szulo();
        szulo gy = new gyerek();

        sz.szuloUzen();
        gy.gyerekUzen();

        return 0;
    }
};