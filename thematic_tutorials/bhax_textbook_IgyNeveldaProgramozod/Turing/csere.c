#include <stdio.h>

int main()
{
    int a = 536;
    int b = 432;

    printf("a = %d, b = %d\n", a, b);

    b = b -a;
    a = a + b;
    b = a - b;

    printf("a = %d, b = %d\n", a, b);

    return 0;
}