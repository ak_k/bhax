<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Chomsky!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Decimálisból unárisba átváltó Turing gép</title>
        <para>
            Megoldás videó:
            <link xlink:href="https://youtu.be/kHaMXMHdVNo">https://youtu.be/kHaMXMHdVNo</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/dec.cpp">https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/dec.cpp</link>
        </para>
        <para>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/turing_gep.png" format="PNG" scale="40"/>
                </imageobject>
                <caption>
                    <para>
                        Decimálisból unárisba átváltó Turing-gép.
                    </para>
                </caption>
            </mediaobject>
            A Turing-gépet Alan Turing angol matematikus dolgozta ki 1936-ban.
            Célja a matemaitkai számítási eljárások és algoritmusok leírása, valamint a jövőbeli számítógépek akkori modellezése.
            A Turing-gép valójában egy absztrakt automata - egy valódi digitális számítógép leegyszerűsített modellje; ezen kívül még egy univerzális algoritmikus modellként is tekinthetünk rá.
            Az ábrán látható Turing-gép bemenetként kap egy decimális (10-es számrendszerű) számot, amit átvált unáris számra.
            Az unáris számrendszer lentebb ki van fejtve.
        </para>
        <para>
            (Bevezetés forrása: Turing-gép c. Wikipedia szócikk.)
        </para>
        <para>
            Az átváltó meg lett írva C++-ban is, hogy szemléltetve legyen a működése.
        </para>
        <para>
            Az unáris számrendszer a legegyszerűbb olyan számrendszer, amelyben természetes számokat ábrázolhatunk.
            Vegyünk egy tetszőleges 'N' természetes számot.
            Ezt az 'N' számot úgy ábrázoljuk unáris számrendszerben, hogy N-szer vesszük az 1-est jelölő szimbólumot (a programban egy a '|' lett).
            Így megkapjuk a kiválasztott szám unáris alakját.
        </para>
        <para>
            A programban először is két változót veszek fel, az 'a'-t, valamint a 'db'-ot; az előbbi az átváltandó szám lesz, az utóbbi pedig egy számláló lesz, erre majd a későbbiekben lesz szükség.
            Megtörténik egy szám beolvasása, majd ezután indul egy 'for' ciklus.
            A ciklus 0-tól indul, és 'a'-ig megy.
            A magban annyiszor fog kiírásra kerülni az 1-est jelentő szimbólum, ahányszor lefut a ciklus, azaz ez a bekért szám nagyságától függ.
            Minden ötödik kiírt "vonal" után a program beszúr egy szóközt az átláthatóság érdekében.
        </para>
    </section>        
        
    <section>
        <title>Az a<superscript>n</superscript>b<superscript>n</superscript>c<superscript>n</superscript> nyelv nem környezetfüggetlen</title>
        <para>
            Mutass be legalább két környezetfüggő generatív grammatikát, amely ezt a nyelvet generálja!
        </para>
        <para>
            Megoldás videó: N/A
        </para>
        <para>
            Megoldás forrása: N/A
        </para>
        <para>
            Generatív nyelvtanról a Chomsky-féle nyelvosztályok kapcsán beszélünk.
            Ő négy nyelvosztályt különböztetett meg: reguláris, környezetfüggetlen, környezetfüggő és rekurtívé felsorolható.
            A környezetfüggő generatív grammatikában a bal, valamint a jobb oldalon is szerepelhetnek termináis szimbólumok.
        </para>
        <para>
            Egyik környezetfüggő példa:
            <programlisting>
<![CDATA[
S, X, Y "változók" - ezek nemterminálisok
a, b, c "konstansok" - ezek terminálisok
S -> abc, S -> aXbc, Xb -> bX, Xc -> Ybcc, bY -> Yb, aY -> aaX, aY -> aa - ezek a képzési szabályok
S lesz a kezdőszimbólum
Ha jól alkalmazzuk a képzési szabályainkat, akkor ezt kapjuk: 
                        
                S (S -> aXbc)
                aXbc (Xb -> bX)
                abXc (Xc -> Ybcc)
                abYbcc (bY -> Yb)
                aYbbcc (aY - aa)
                aabbcc
]]>
            </programlisting>
            Az, hogy ez a grammatika környezetfüggő, kiderül abból, hogy a képzési szabályokban előfordul olyan, hogy a nyíl bal, valamint jobb oldalán is van terminális szimbólum.
        </para>
        <para>
            Másik környezetfüggő példa:
            <programlisting>
<![CDATA[
A, B, C "változók" - ezek nemterminálisok
a, b, c "konstansok" - ezek terminálisok
A -> aAB, A -> aC, CB -> bCc, cB -> Bc, C -> bc - ezek a képzési szabályok
A lesz a kezdőszimbólum
Ha jól alkalmazzuk a képzési szabályainkat, akkor ezt kapjuk: 
                
                A (A -> aAB)
                aAB (A -> aC)
                aaCB (CB -> bCc)
                aabCc (C -> bc)
                aabbcc
]]>
            </programlisting>
        </para>

    </section>        
                
    <section>
        <title>Hivatkozási nyelv</title>
        <para>
            A <citation>KERNIGHANRITCHIE</citation> könyv C referencia-kézikönyv/Utasítások melléklete alapján definiáld 
            BNF-ben a C utasítás fogalmát!
            Majd mutass be olyan kódcsipeteket, amelyek adott szabvánnyal nem fordulnak (például C89), mással (például C99) igen.
        </para>
        <para>
            Megoldás videó:
            <link xlink:href="https://youtu.be/hf-2oNu6Tz0">https://youtu.be/hf-2oNu6Tz0</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/for89.c">https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/for89.c</link>, <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/for99.c">https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/for99.c</link>
        </para>
        <para>
            Az alábbi kódrészlet a C99-es szabvány szerint helyes, azonban C89-es - másnéven ANSI C - szabványt használva nem.
            <programlisting language="c">
<![CDATA[
    #include <stdio.h>

    int main()
    {
        for (int i = 0; i < 10; i++) 
        {
            printf("i = %d\n", i);
        }

        return 0;
    }
]]>
            </programlisting>
        </para>
        <para>
            Ennek az az oka, hogy a 89-es szabvány még nem támogadta, hogy egy for ciklus fejében deklaráljuk a ciklusváltozót; még fordításnál hibát dob a compiler.
            A 99-es szabványnak viszont már semmi gondja nincs azzal, ha a ciklusfejben történik meg a deklaráció, simán lefordul tőle a program.
        </para>
        <para>
            A C89-es szabványt követve így lenne helyes a program:
            <programlisting language="c">
<![CDATA[
    #include <stdio.h>

    int main()
    {
        int i;

        for (i = 0; i < 10; i++) 
        {
            printf("i = %d\n", i);
        }

        return 0;
    }
]]>
            </programlisting>
        </para>
    </section>                     

    <section>
        <title>Saját lexikális elemző</title>
        <para>
            Írj olyan programot, ami számolja a bemenetén megjelenő valós számokat! 
            Nem elfogadható olyan megoldás, amely maga olvassa betűnként a bemenetet, 
            a feladat lényege, hogy lexert használjunk, azaz óriások vállán álljunk és ne kispályázzunk!
        </para>
        <para>
            Megoldás videó:
            <link xlink:href="https://youtu.be/HRliMPxvTwI">https://youtu.be/HRliMPxvTwI</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/realnumber.l">https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/realnumber.l</link> 
        </para>
        <programlisting language="c"><![CDATA[%{
#include <stdio.h>
int realnumbers = 0;
%}
digit	[0-9]
%%
{digit}*(\.{digit}+)?	{++realnumbers; 
    printf("[realnum=%s %f]", yytext, atof(yytext));}
%%
int
main ()
{
 yylex ();
 printf("The number of real numbers is %d\n", realnumbers);
 return 0;
}
]]></programlisting>
        <para>
            Az adott lexert felhasználva a 'Flex' nevű program segítségével legeneráljuk a C forrást.
            Ezt a következő képpen tehetjük meg:
            <programlisting>
<![CDATA[
lex -o realnumber.c realnumber.l
]]>
            </programlisting>
        </para>
        <para>
            Ezután már a megszokott módon fordítjuk a forrást:
            <programlisting>
<![CDATA[
gcc realnumber.c -o realnumber
]]>          
            </programlisting>
        </para>
    </section>                     

    <section>
        <title>Leetspeak</title>
        <para>
            Lexelj össze egy l33t ciphert!
        </para>
        <para>
            Megoldás videó:
            <link xlink:href="https://youtu.be/o5imzSdYaac">https://youtu.be/o5imzSdYaac</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/l337d1c7.l">
                https://gitlab.com/ak_k/bhax/-/blob/master/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/l337d1c7.l
            </link>  
        </para>
        <programlisting language="c"><![CDATA[%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <time.h>
  #include <ctype.h>

  #define L337SIZE (sizeof l337d1c7 / sizeof (struct cipher))
    
  struct cipher {
    char c;
    char *leet[4];
  } l337d1c7 [] = {

  {'a', {"4", "4", "@", "/-\\"}},
  {'b', {"b", "8", "|3", "|}"}},
  {'c', {"c", "(", "<", "{"}},
  {'d', {"d", "|)", "|]", "|}"}},
  {'e', {"3", "3", "3", "3"}},
  {'f', {"f", "|=", "ph", "|#"}},
  {'g', {"g", "6", "[", "[+"}},
  {'h', {"h", "4", "|-|", "[-]"}},
  {'i', {"1", "1", "|", "!"}},
  {'j', {"j", "7", "_|", "_/"}},
  {'k', {"k", "|<", "1<", "|{"}},
  {'l', {"l", "1", "|", "|_"}},
  {'m', {"m", "44", "(V)", "|\\/|"}},
  {'n', {"n", "|\\|", "/\\/", "/V"}},
  {'o', {"0", "0", "()", "[]"}},
  {'p', {"p", "/o", "|D", "|o"}},
  {'q', {"q", "9", "O_", "(,)"}},
  {'r', {"r", "12", "12", "|2"}},
  {'s', {"s", "5", "$", "$"}},
  {'t', {"t", "7", "7", "'|'"}},
  {'u', {"u", "|_|", "(_)", "[_]"}},
  {'v', {"v", "\\/", "\\/", "\\/"}},
  {'w', {"w", "VV", "\\/\\/", "(/\\)"}},
  {'x', {"x", "%", ")(", ")("}},
  {'y', {"y", "", "", ""}},
  {'z', {"z", "2", "7_", ">_"}},
  
  {'0', {"D", "0", "D", "0"}},
  {'1', {"I", "I", "L", "L"}},
  {'2', {"Z", "Z", "Z", "e"}},
  {'3', {"E", "E", "E", "E"}},
  {'4', {"h", "h", "A", "A"}},
  {'5', {"S", "S", "S", "S"}},
  {'6', {"b", "b", "G", "G"}},
  {'7', {"T", "T", "j", "j"}},
  {'8', {"X", "X", "X", "X"}},
  {'9', {"g", "g", "j", "j"}}
  
// https://simple.wikipedia.org/wiki/Leet
  };
  
%}
%%
.	{
	  
	  int found = 0;
	  for(int i=0; i<L337SIZE; ++i)
	  {
	  
	    if(l337d1c7[i].c == tolower(*yytext))
	    {
	    
	      int r = 1+(int) (100.0*rand()/(RAND_MAX+1.0));
	    
          if(r<91)
	        printf("%s", l337d1c7[i].leet[0]);
          else if(r<95)
	        printf("%s", l337d1c7[i].leet[1]);
	      else if(r<98)
	        printf("%s", l337d1c7[i].leet[2]);
	      else 
	        printf("%s", l337d1c7[i].leet[3]);

	      found = 1;
	      break;
	    }
	    
	  }
	  
	  if(!found)
	     printf("%c", *yytext);	  
	  
	}
%%
int 
main()
{
  srand(time(NULL)+getpid());
  yylex();
  return 0;
}
]]></programlisting>
        <para>
            Ennek a programnak a létrejötte hasonló, az előzőhöz.
            A forráskódot ismételten egy lexer fogja generálni, a már ismert módón:
            <programlisting>
<![CDATA[
lex -o l337d1c7.c l337d1c7.l
]]>
            </programlisting>
        </para>
        <para>
            Ezek után már csak a fordítás van hátra:
            <programlisting>
<![CDATA[
gcc l337d1c7.c -o l337d1c7
]]>
            </programlisting>
        </para>
        <para>
            A program maga sem túl bonyolult.
            Egy beolvasott stringben a megfelelő karaktereket lecseréli a tömbben definiált elemek valamelyikére.
            Azt, hogy a több lehetőség közül melyiket választja, azt egy random szám generálással határozza meg.
        </para>
    </section>                     


    <section>
        <title>A források olvasása</title>
        <para>
            Hogyan olvasod, hogyan értelmezed természetes nyelven az alábbi kódcsipeteket? Például
            <programlisting><![CDATA[if(signal(SIGINT, jelkezelo)==SIG_IGN)
    signal(SIGINT, SIG_IGN);]]></programlisting>
            Ha a SIGINT jel kezelése figyelmen kívül volt hagyva, akkor ezen túl is legyen
            figyelmen kívül hagyva, ha nem volt figyelmen kívül hagyva, akkor a jelkezelo függvény
            kezelje. (Miután a <command>man 7 signal</command> lapon megismertem a SIGINT jelet, a
            <command>man 2 signal</command> lapon pedig a használt rendszerhívást.)
        </para>

        <caution>
            <title>Bugok</title>
            <para>
                Vigyázz, sok csipet kerülendő, mert bugokat visz a kódba! Melyek ezek és miért? 
                Ha nem megy ránézésre, elkapja valamelyiket esetleg a splint vagy a frama?
            </para>
        </caution>
            
        <orderedlist numeration="lowerroman">   
            <listitem>
            <para>
                Az alábbi kódrészlet szerint, ha eddig nem volt figyelmen kívül hagyva a SIGINT, akkor jelkezelo függvény lekezeli, egyébként ha figyelmen kívül vol hagyva, akkor ne csináljon vele semmit.
            </para>                               
                <programlisting><![CDATA[if(signal(SIGINT, SIG_IGN)!=SIG_IGN)
    signal(SIGINT, jelkezelo);]]></programlisting>
            </listitem>
            <listitem>
                <para>
                    Az 'i' ciklusváltozó értéke 0-ra lesz beállítva; a ciklusmagban megadott utasítás addig fog végrehajtódni, amíg az 'i' nem lesz 5; majd az 'i' értékét megnöveli 1-el.
                </para>
                <programlisting><![CDATA[for(i=0; i<5; ++i)]]></programlisting>            
            </listitem>
            <listitem>                                    
                <para>
                    Az 'i' ciklusváltozó értéke 0-ra lesz beállítva; a ciklusmagban megadott utasítás addig fog végrehajtódni, amíg az 'i' nem lesz 5; majd az 'i' értékét megnöveli 1-el.
                </para>
                <programlisting><![CDATA[for(i=0; i<5; i++)]]></programlisting>
                <para>
                    Az előző két kódrészlet ugyanazt csinálja, egyedül az értékadásnál van különbség.
                    Ha egy változónak értéknek adjuk az 'i++'-t, akkor az említett változó először felveszi az 'i' értékét, majd az 'i' értéke 1-el nő.
                    Ha egy változónak értéknek adjuk a '++i'-t, akkor először az 'i' értéke fog nőni 1-el, majd ez a növelt értéket fogja megkapni az adott változó, mint új értéket.
                </para>          
            </listitem>
            <listitem>                                    
                <para>
                    A ciklus 0-tól indul és addig megy, amígy 'i' értéke 5 nem lesz; egy 'tomb' nevű tömb 'i'-edik eleme megekapja értéknek 'i' értékét, ami azután 1-el lesz növelve.
                    A ciklusmagban lévő utasítás 5-ször fog végrehajtódni.
                </para>
                <programlisting><![CDATA[for(i=0; i<5; tomb[i] = i++)]]></programlisting>            
            </listitem>
            <listitem>                                    
                <para>
                    A ciklus 0-tól indul, és addig megy, amíg az 'i' értéke kisebb, mint 'n' értéke, valamint a '*d++' egyenlő a '*s++'.
                </para>
                <programlisting><![CDATA[for(i=0; i<n && (*d++ = *s++); ++i)]]></programlisting>            
            </listitem>
            <listitem>                                    
                <para>
                    Kiiratásra kerül az 'f' függvény által visszaadott két érték.
                </para>
                <programlisting><![CDATA[printf("%d %d", f(a, ++a), f(++a, a));]]></programlisting>            
            </listitem>
            <listitem>                                    
                <para>
                    Kiiratásra kerül két érték; először az 'f' függvény által visszaadott érték, majd az 'a' értéke.
                </para>
                <programlisting><![CDATA[printf("%d %d", f(a), a);]]></programlisting>            
            </listitem>
            <listitem>                                    
                <para>
                    Kiiratásra kerül két érték; először az 'f' függvény által visszaadott érték memóricíme, majd 'a' értéke.
                </para>
                <programlisting><![CDATA[printf("%d %d", f(&a), a);]]></programlisting>            
            </listitem>
        </orderedlist>
        <para>
            Megoldás forrása: N/A
        </para>

        <para>
            Megoldás videó: N/A
        </para>

    </section>                     

    <section>
        <title>Logikus</title>
        <para>
            Hogyan olvasod természetes nyelven az alábbi Ar nyelvű formulákat?
        </para>
        <programlisting language="tex"><![CDATA[$(\forall x \exists y ((x<y)\wedge(y \text{ prím})))$ 

$(\forall x \exists y ((x<y)\wedge(y \text{ prím})\wedge(SSy \text{ prím})))$ 

$(\exists y \forall x (x \text{ prím}) \supset (x<y)) $ 

$(\exists y \forall x (y<x) \supset \neg (x \text{ prím}))$
]]></programlisting>        
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/ak_k/bhax/-/tree/master/attention_raising/MatLog_LaTeX">https://gitlab.com/ak_k/bhax/-/tree/master/attention_raising/MatLog_LaTeX</link>
        </para>

        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/tVCdIjU9KO0">https://youtu.be/tVCdIjU9KO0</link>
        </para>

        <para>
            A megadott Ar formulák természtes nyelvű olvasata:
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/ar.png" format="PNG"/>
                </imageobject>
            </mediaobject>
            A megadott formulákat LaTeX-szel tudjuk értelmezni, ez a következő módon történik:
            <programlisting>
<![CDATA[
    latex fajlnev.tex
]]>
            </programlisting>
            A LaTeX így generál egy emberileg is olvasható .dvi fájlt, amiben megtalálható a fentebbi kiemelt részlet.
        </para>
    </section>                                                                                                                                                                            
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
    <section>
        <title>Deklaráció</title>
            
        <para>
            Vezesd be egy programba (forduljon le) a következőket: 
        </para>

        <itemizedlist>
            <listitem>
                <para>egész</para>
                <para>
                    <programlisting>
<![CDATA[int a = 42;]]>
                    </programlisting>
                </para>                    
            </listitem>
            <listitem>
                <para>egészre mutató mutató</para>
                <para>
                    <programlisting>
<![CDATA[int *b = &a;]]>
                    </programlisting>
                </para>                   
            </listitem>
            <listitem>
                <para>egész referenciája</para>
                <para>
                    <programlisting>
<![CDATA[int &ref = a;]]>
                    </programlisting>
                </para>                     
            </listitem>
            <listitem>
                <para>egészek tömbje</para>
                <para>
                    <programlisting>
<![CDATA[int t[42];]]>
                    </programlisting>
                </para>                      
            </listitem>
            <listitem>
                <para>egészek tömbjének referenciája (nem az első elemé)</para>
                <para>
                    <programlisting>
<![CDATA[int (&ref)[5] = t;]]>
                    </programlisting>
                </para>                         
            </listitem>
            <listitem>
                <para>egészre mutató mutatók tömbje</para>
                <para>
                    <programlisting>
<![CDATA[int *t[5];]]>
                    </programlisting>
                </para>                      
            </listitem>
            <listitem>
                <para>egészre mutató mutatót visszaadó függvény</para>      
                <para>
                    <programlisting>
<![CDATA[int *f();]]>
                    </programlisting>
                </para>                    
            </listitem>
            <listitem>
                <para>egészre mutató mutatót visszaadó függvényre mutató mutató</para>                    
                <para>
                    <programlisting>
<![CDATA[int *(*f2)();]]>
                    </programlisting>
                </para>  
            </listitem>
            <listitem>
                <para>egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvény</para>                        
                <para>
                    <programlisting>
<![CDATA[int (*f3(int a)) (int b, int c);]]>
                    </programlisting>
                </para>  
            </listitem>            
            <listitem>
                <para>függvénymutató egy egészet visszaadó és két egészet kapó függvényre mutató mutatót visszaadó, egészet kapó függvényre</para>                        
                <para>
                    <programlisting>
<![CDATA[int (*(f4)(int)) (int, int);]]>
                    </programlisting>
                </para>  
            </listitem>            
        </itemizedlist>            

        <para>
            Mit vezetnek be a programba a következő nevek?
        </para>

        <itemizedlist>
            <listitem>
                <para>
                    Egész típusó változó.
                </para>
                <programlisting><![CDATA[int a;]]></programlisting>            
            </listitem>
            <listitem>
                <para>
                    Egész típusó pointer, ami 'a'-ra mutat.
                </para>
                <programlisting><![CDATA[int *b = &a;]]></programlisting>            
            </listitem>
            <listitem>
                <para>
                    Egész referencia, ami 'a'-ra hivatokzik.
                </para>
                <programlisting><![CDATA[int &r = a;]]></programlisting>            
            </listitem>
            <listitem>
                <para>
                    Egész típusú tömb, melynek mérete 5.
                </para>
                <programlisting><![CDATA[int c[5];]]></programlisting>            
            </listitem>
            <listitem>
                <para>
                    Egész típusú tömb referenciája, melynek mérete 5 és a 'c' nevű tömb címére hivatkozik.
                </para>
                <programlisting><![CDATA[int (&tr)[5] = c;]]></programlisting>            
            </listitem>
            <listitem>
                <para>
                    Egészekre mutató pointerekből álló tömb, melynek mérete 5.
                </para>
                <programlisting><![CDATA[int *d[5];]]></programlisting>            
            </listitem>
            <listitem>
                <para>
                    Paramétermentes függvény, ami egészre mutató pointert ad vissza.
                </para>
                <programlisting><![CDATA[int *h ();]]></programlisting>          
            </listitem>
            <listitem>
                <para>
                    Egészre mutató pointert visszaadó, paraméteres függvényre mutató pointer.
                </para>
                <programlisting><![CDATA[int *(*l) ();]]></programlisting>            
            </listitem>
            <listitem>
                <para>
                    Egészet visszaadó és két egészet bekérő függvényre mutató pointert visszaadó egészet bekérő függvény.
                </para>
                <programlisting><![CDATA[int (*v (int c)) (int a, int b)]]></programlisting>            
            </listitem>            
            <listitem>
                <para>
                    Egészet visszaadó és két egészet bekérő függvényre mutató pointert visszaadó, egészet bekérő függvényre mutató pointer.
                </para>
                <programlisting><![CDATA[int (*(*z) (int)) (int, int);]]></programlisting>            
            </listitem>            
        </itemizedlist>       


        <para>
            Megoldás videó: 
            <link xlink:href="https://youtu.be/tC31vrvfYAg">https://youtu.be/tC31vrvfYAg</link>
        </para>
        <para>
            Megoldás forrása:  
        </para>
        <para>
            Az utolsó két deklarációs példa demonstrálására két olyan kódot
            írtunk, amelyek összahasonlítása azt mutatja meg, hogy miért 
            érdemes a <command>typedef</command> használata: <link xlink:href="Chomsky/fptr.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/fptr.c</filename>
            </link>,
            <link xlink:href="Chomsky/fptr2.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Chomsky/fptr2.c</filename>.
            </link>  
            
        </para>
        <programlisting><![CDATA[#include <stdio.h>

int
sum (int a, int b)
{
    return a + b;
}

int
mul (int a, int b)
{
    return a * b;
}

int (*sumormul (int c)) (int a, int b)
{
    if (c)
        return mul;
    else
        return sum;

}

int
main ()
{

    int (*f) (int, int);

    f = sum;

    printf ("%d\n", f (2, 3));

    int (*(*g) (int)) (int, int);

    g = sumormul;

    f = *g (42);

    printf ("%d\n", f (2, 3));

    return 0;
}]]></programlisting>        
        <para>
            A fentebbi kód egy példa arra, hogyan is lehet a korábbi deklarációkat alkalmazni.
            A 'main' elején deklarálva van egy két egészet bekérő és egyet visszaadó függvény, ami egészre mutató pointert ad vissza.
            <programlisting>
<![CDATA[int (*f) (int, int);]]>
            </programlisting>
        </para>    
        <para>
            Erre a pointerre lesz meghívva a 'sum' függvény, amit a kiíratásnál is láthatunk.
            <programlisting>
<![CDATA[f = sum;
printf("%d\n", f(2, 3))]]>
            </programlisting>
            Ez azt eredményezi, hogy a 'sum' függvény megkapja paraméterként a 2-t és a 3-t, és ezeknek az összegét adja vissza.
        </para>
        <para>
            A program második fele is hasonlóan működik, viszont itt egy kicsit más a deklaráció, és összeadás helyett szorzás van.ű
            Ennek a résznek az elején van egy két egészet bekérő és egy egészet visszaadó függvényre mutató pointert visszaadó, egészet bekérő függvényre mutató pointer.
            <programlisting>
<![CDATA[int (*(*g) (int)) (int, int);]]>
            </programlisting>
        </para>
        <programlisting><![CDATA[#include <stdio.h>

typedef int (*F) (int, int);
typedef int (*(*G) (int)) (int, int);

int
sum (int a, int b)
{
    return a + b;
}

int
mul (int a, int b)
{
    return a * b;
}

F sumormul (int c)
{
    if (c)
        return mul;
    else
        return sum;
}

int
main ()
{

    F f = sum;

    printf ("%d\n", f (2, 3));

    G g = sumormul;

    f = *g (42);

    printf ("%d\n", f (2, 3));

    return 0;
}
]]></programlisting>            
        <para>
            Ez a kód ugyanazt hajtja végre, csak a deklarációk mások.
            Az előző példában a függvényeket lokálisan deklaráltuk, azaz csak az adott scope-ban volt elérhető.
            Itt viszont ezt globálisan tesszük meg, azaz a program bármely pontján hozzájuk férünk.
        </para>
        <para>
            Két egészet bekérő és egy egészet visszaadó függvény, ami egészre mutató pointert ad vissza:
            <programlisting>
<![CDATA[typedef int (*F) (int, int);]]>
            </programlisting>
        </para>
        <para>
            Két egészet bekérő és egy egészet visszaadó függvényre mutató pointert visszaadó, egészet bekérő függvényre mutató pointer:
            <programlisting>
<![CDATA[typedef int (*(*G) (int)) (int, int);]]>
            </programlisting>
        </para>
    </section>                     
    <section>
        <title>Vörös Pipacs Pokol/csiga diszkrét mozgási parancsokkal</title>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/Fc33ByQ6mh8">https://youtu.be/Fc33ByQ6mh8</link>      
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://github.com/nbatfai/RedFlowerHell">https://github.com/nbatfai/RedFlowerHell</link>               
        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat... ezt kell az olvasónak kidolgoznia, mint labor- vagy otthoni mérési feladatot!
            Ha mi már megtettük, akkor használd azt, dolgozd fel, javítsd, adj hozzá értéket!
        </para>            
    </section>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
</chapter>                
