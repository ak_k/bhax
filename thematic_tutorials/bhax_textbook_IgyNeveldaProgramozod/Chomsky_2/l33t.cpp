#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>

using namespace std;

class l33tCypher
{
    public:
        l33tCypher();
        string decypher (string& text);

    private:
    	vector<vector <string>> l33tAlphabet;
    	vector<string> engAlphabet;
};

l33tCypher::l33tCypher()
{
	srand (time(NULL));

	l33tAlphabet.resize(26);
	l33tAlphabet[0] = {"4", "/-\\", "/_\\", "@", "/\\"};
	l33tAlphabet[1] = {"8","|3", "13", "|}", "|:", "|8", "18", "6", "|B", "|8", "lo", "|o"};
	l33tAlphabet[2] = {"<", "{", "[", "("};
	l33tAlphabet[3] = {"|)", "|}", "|]"};
	l33tAlphabet[4] = {"3"};
	l33tAlphabet[5] = { "|=", "ph", "|#", "|\""};
	l33tAlphabet[6] = {"[", "-", "[+", "6"};
	l33tAlphabet[7] = {"4", "|-|", "[-]", "{-}", "|=|", "[=]", "{=}"};
	l33tAlphabet[8] = {"1", "|", "!", "9"};
	l33tAlphabet[9] = {"_|", "_/", "_7", "_)", "_]", "_}"};
	l33tAlphabet[10] = {"|<", "1<", "l<", "|{", "l{"};
	l33tAlphabet[11] = {"|_", "|", "1", "]["};
	l33tAlphabet[12] = {"44", "|\\/|", "^^", "/\\/\\", "/X\\", "[]\\/][", "[]V[]", "][\\\\//][", "(V)","//., .\\\\", "N\\"};
	l33tAlphabet[13] = {"|\\|", "/\\/", "/V", "][\\\\]["};
	l33tAlphabet[14] = {"0", "()", "[]", "{}", "<>"};
	l33tAlphabet[15] = { "|o", "|O", "|>", "|*", "|\textdegree{}", "|D", "/o"};
	l33tAlphabet[16] = {"O_", "9", "(,)", "0,"};
	l33tAlphabet[17] = {"|2"," 12", ".-", "|^", "l2"};
	l33tAlphabet[18] = {"5", "$", "§"};
	l33tAlphabet[19] = {"7", "+", "7‘", "\'|\'" , "‘|‘" , "~|~" , "-|-"};
	l33tAlphabet[20] = {"|_|", "\\_\\", "/_/", "\\_/", "(_)", "[_]", "{_}"};
	l33tAlphabet[21] = {"\\/"};
	l33tAlphabet[22] = {"\\/\\/", "(/\\)", "\\^/", "|/\\|", "\\X/", "\\\\\'", "\'//", "VV"};
	l33tAlphabet[23] = {"%", "*", "><", "}{", ")("};
	l33tAlphabet[24] = {"‘/", "¥"};
	l33tAlphabet[25] = { "2", "7_", ">_"};

	engAlphabet = 
	{
		"A",
		"B",
		"C",
		"D",
		"E",
		"F",
		"G",
		"H",
		"I",
		"J",
		"K",
		"L",
		"M",
		"N",
		"O",
		"P",
		"Q",
		"R",
		"S",
		"T",
		"U",
		"V",
		"W",
		"X",
		"Y",
		"Z"
	};
}

string l33tCypher::decypher(string &text)
{
	string l33t = "";
	string eng = text;

	transform(eng.begin(), eng.end(), eng.begin(), ::toupper);

	while (!eng.empty())
	{
		bool find = false;

		for (int i = 1; i < engAlphabet.size(); i++)
		{
			size_t found = eng.find(engAlphabet[i]);

			if (found == 0)
			{
				find = true;

				l33t += l33tAlphabet[i][rand() % l33tAlphabet[i].size()];

				if (eng.length() > 1)
				{
					eng = eng.substr(1);
				}
				else
				{
					eng.clear();
				}
			}
		}
		if (!find)
		{
			l33t += eng[0];

			if (eng.length() > 1)
			{
				eng = eng.substr(1);
			}
			else
			{
				eng.clear();
			}
		}
	}

	return l33t;
}

int main(int argc, char * argv[])
{
	ifstream infile (argv[1]);
	fstream outfile (argv[2], ios::out);

	string text = "";
	string temp = "";

	while (getline(infile, temp))
	{
		text += temp;
	}

	l33tCypher* cypher = new l33tCypher();

	string cypheredText = cypher->decypher(text);
	outfile << cypheredText << endl;
}